"use strict";

/// <binding ProjectOpened='watch' />
/** This line is needed so the 'watch' runs when the solution opens **/

/** If you encounter an error and the 'watch' task stops you need to run it manually via Task Runner Explorer **/



// Load plugins
const del = require("del");
const { src, dest, series, parallel, watch } = require("gulp");
const rename = require("gulp-rename");
const sass = require("gulp-sass");
const babel = require("gulp-babel");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify");
const notify = require("gulp-notify");
const sourcemaps = require("gulp-sourcemaps");
//const pipeline = require("readable-stream").pipeline;
//const postcss = require("gulp-postcss");
//const autoprefixer = require("autoprefixer");
//const cssnano = require("cssnano");



// Store CSS and JS directories in object
const dist = {

  css: [
    './src/scss/**/*.css',
    './src/scss/**/*scss'
  ],
  js: [
      './src/js/jquery.3.4.1.js', // Concatenates jQuery first as a dependency
      './src/js/**/*.js'
  ]

};

// Store bundle destinations in variables
const cssDest = './dist/css';
const jsDest = './dist/js';



// Clean dist folder
function clean() {
  return del([cssDest, jsDest]);
}



// Run CSS Build
function css() {
  return src(dist.css)
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: "expanded" }).on("error", sass.logError))
    //.pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(sourcemaps.write('.', {includeContent: false}))
    .pipe(dest(cssDest))
    .pipe(notify({ message: 'Styles created and minified'}));
}



// Run JS Build
function scripts() {
  return src(dist.js)
    .pipe(babel({presets: ['@babel/env']}))
    .pipe(concat('bundle.js'))
    .pipe(uglify())
    .pipe(dest(jsDest))
    .pipe(notify({ message: 'Scripts created and minified'}));
}



// Watch files
function watchFiles() {
  watch(dist.css, css);
  watch(dist.js, scripts);
}



// Set tasks to variables
const js = scripts;
const build = series(clean, parallel(css, scripts));
const watcher = series(clean, parallel(css, scripts), watchFiles);



// Export all tasks
exports.css = css;
exports.js = js;
exports.clean = clean;
exports.build = build;
exports.watch = watcher;
exports.default = watcher;